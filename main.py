from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from routers import router

app = FastAPI(
    title="API Price Habi",
    description="Get Price",
    version="0.1"
)

app.add_middleware(
    CORSMiddleware,
    allow_origins='*',
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
	expose_headers=["*"],
)

app.include_router(router)
# uvicorn main:app --reload
