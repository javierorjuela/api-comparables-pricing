from fastapi import APIRouter
from endpoints import get_info_comparables, home_page, get_info_api

router = APIRouter()

router.include_router(home_page.router, tags=["Home"])
router.include_router(get_info_comparables.router, tags=["get_info_comparables"])
router.include_router(get_info_api.router, tags=["get_info"])