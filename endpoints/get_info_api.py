from fastapi import Security, Depends, FastAPI, HTTPException
from fastapi.security.api_key import APIKeyQuery, APIKeyCookie, APIKeyHeader, APIKey
from fastapi.openapi.docs import get_swagger_ui_html, get_redoc_html
from fastapi.openapi.utils import get_openapi
from starlette.status import HTTP_403_FORBIDDEN
from starlette.responses import RedirectResponse, JSONResponse
from fastapi.middleware import Middleware
from fastapi import APIRouter
from endpoints.config_get_info_api import get_info
from api_functions import ResponseModel, ErrorResponseModel
from fastapi.responses import JSONResponse
import logging, traceback

logger = logging.getLogger()
logger.setLevel(logging.INFO)

API_KEY_NAME = "x-api-key"
API_KEY = "64mSHcX6goIUOyAZBYhKdANPk"

api_key_query = APIKeyQuery(name=API_KEY_NAME, auto_error=False)
api_key_header = APIKeyHeader(name=API_KEY_NAME, auto_error=False)

router = APIRouter()

async def get_api_key(
	api_key_query: str = Security(api_key_query),
	api_key_header: str = Security(api_key_header)):

	if api_key_query == API_KEY:
		return api_key_query
	elif api_key_header == API_KEY:
		return api_key_header
	else:
		raise HTTPException(
			status_code=HTTP_403_FORBIDDEN, detail="Could not validate credentials"
		)

app = FastAPI(docs_url=None, redoc_url=None, openapi_url=None)

router = APIRouter()

@router.get("/get_info", name="Property Info", description="Returns Property Info")
async def get(nid, api_key: APIKey = Depends(get_api_key)):
    # Pipeline de ejecución
        try:
            json_info = get_info(nid)
            return json_info        
        except Exception as e:
            return ErrorResponseModel(422, "Services has failed to execute.")
