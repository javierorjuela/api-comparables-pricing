import json
from google.cloud import secretmanager
from sqlalchemy import create_engine


def load_credentials(name_secret):
    # # GCP project in which to store secrets in Secret Manager.
    project_id = "papyrus-data"

    # # ID of the secret to create.
    secret_id = f"projects/224992036374/secrets/{name_secret}/versions/latest"

    # Create the Secret Manager client.
    client = secretmanager.SecretManagerServiceClient()

    # Access the secret version.
    response = client.access_secret_version(request={"name": secret_id})

    # Print the secret payload.
    #
    # WARNING: Do not print the secret in a production environment - this
    # snippet is showing how to access the secret material.
    payload = response.payload.data.decode("UTF-8")
    #Save in dictionary
    cred = json.loads(payload)
    
    return cred

secret=load_credentials('DataTeamUser1')
secret2=load_credentials('papyrus-data-cronjobs')

user = secret['user']
password = secret['password']
schema = 'habi_db'
database = "habi_db"
host = secret['host']
port = secret['port']
engine = create_engine(
		f'mysql+mysqlconnector://{user}:{password}@{host}:{port}/{schema}')