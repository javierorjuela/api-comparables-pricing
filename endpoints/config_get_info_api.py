import pandas as pd
import numpy as np
import mysql.connector as sql
from sqlalchemy import create_engine
from google.cloud import bigquery
from google.oauth2 import service_account
from endpoints.read_secret import load_credentials

secret2=load_credentials('papyrus-data-cronjobs')

#Bigquery credential
credential =  {"type": secret2['type'],
            "project_id": secret2['project_id'],
            "private_key_id": secret2['private_key_id'],
            "private_key": secret2['private_key'].replace('\\n','\n'),
            "client_email": secret2['client_email'],
            "client_id": secret2['client_id'],
            "auth_uri": secret2['auth_uri'],
            "token_uri": secret2['token_uri'],
            "auth_provider_x509_cert_url": secret2['auth_provider_x509_cert_url'],
            "client_x509_cert_url": secret2['client_x509_cert_url']}

def read_query_bq(query, credential):
        try:
            credentials = service_account.Credentials.from_service_account_info(credential)
            client = bigquery.Client(project=credential['project_id'], credentials=credentials)
            result = client.query(query).to_dataframe()
        except Exception as ex:
            print('Problema de extracción de la información')
            print(ex)
            exception_type, exception_object, exception_traceback = sys.exc_info()
            filename = exception_traceback.tb_frame.f_code.co_filename
            line_number = exception_traceback.tb_lineno

            print("Exception type: ", exception_type)
            print("File name: ", filename)
            print("Line number: ", line_number)
            result = None
        return result

base = pd.read_excel('endpoints/Base.xlsx')

def get_info(nid):
    response = read_query_bq(f'''SELECT tni.nid, tli.lote_id FROM `papyrus-data.habi_wh.tabla_negocio_inmueble` tni 
    JOIN `papyrus-data.habi_wh.tabla_inmueble_v2` ti on tni.inmueble_id = ti.id
    JOIN `papyrus-data.habi_wh.tabla_localizacion_inmueble_v2` tli on ti.localizacion_new_id = tli.id
    WHERE tni.nid={nid}''',credential)

    comparables=base.loc[base['lote_original']==int(response.at[0,'lote_id'])]
    comparables=comparables.reset_index(drop=True)
    lote_original=comparables['lote_original'][0]

    response_2 = read_query_bq(f'''SELECT id,proyecto,unidades_totales,vetustez,deposito,gimnasio,piscina, terraza, lavanderia
    FROM(SELECT
            ROW_NUMBER() OVER (PARTITION BY CAST(cc.id AS INT64) ORDER BY IFNULL(web.deposito,0) + IFNULL(web.gimnasio,0) + IFNULL(web.piscina,0) + IFNULL(web.terraza,0) + IFNULL(web.zonalavanderia,0) DESC) as orden,
            cast(cc.id as INT64) as id,
            cc.proyecto,
            CAST(cc.unidades_totales AS INT64) as unidades_totales,
            CAST(cc.vetustez AS INT64) as vetustez,
            IFNULL(web.deposito,0) as deposito,
            IFNULL(web.gimnasio,0) as gimnasio,
            IFNULL(web.piscina,0) as piscina,
            IFNULL(web.terraza,0) as terraza,
            IFNULL(web.zonalavanderia,0) as lavanderia
            FROM
            `papyrus-data.habi_wh_geodata.conjuntos_colombia` as cc
            LEFT JOIN
            `papyrus-data.habi_wh_geodata.inmuebles_webscraping_match_conjuntos` as mic on cc.conjunto_id = mic.conjunto_id
            LEFT JOIN
            `papyrus-data.habi_wh.webscraping_databd` as web on mic.id_webscraping = web.id
            
            WHERE
            cc.id ={lote_original}
            ORDER BY IFNULL(web.deposito,0) + IFNULL(web.gimnasio,0) + IFNULL(web.piscina,0) + IFNULL(web.terraza,0) + IFNULL(web.zonalavanderia,0) DESC)
    WHERE orden = 1''',credential)
    try:
        out = {
            'id':str(response_2.at[0,'id']),
            'proyecto':str(response_2.at[0,'proyecto']),
            'unidades_totales':str(response_2.at[0,'unidades_totales']),
            'vetustez':str(response_2.at[0,'vetustez']),
            'deposito':str(response_2.at[0,'deposito']),
            'gimnasio':str(response_2.at[0,'gimnasio']),
            'piscina':str(response_2.at[0,'piscina']),
            'terraza':str(response_2.at[0,'terraza']),
            'lavanderia':str(response_2.at[0,'lavanderia'])
        }
    except Exception as e:
        out = {
            'id':'Error',
            'proyecto':'Error',
            'unidades_totales':'Error',
            'vetustez':'Error',
            'deposito':'Error',
            'gimnasio':'Error',
            'piscina':'Error',
            'terraza':'Error',
            'lavanderia':'Error',
            'Error':str(e)
        }
    return out